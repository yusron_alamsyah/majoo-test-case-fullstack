/*
 Navicat Premium Data Transfer

 Source Server         : local-xampp
 Source Server Type    : MySQL
 Source Server Version : 100424
 Source Host           : localhost:3306
 Source Schema         : tes_majoo

 Target Server Type    : MySQL
 Target Server Version : 100424
 File Encoding         : 65001

 Date: 20/08/2022 13:11:25
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for m_admin
-- ----------------------------
DROP TABLE IF EXISTS `m_admin`;
CREATE TABLE `m_admin`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `username` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `password` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `role` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `last_login` datetime NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 20 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_admin
-- ----------------------------
INSERT INTO `m_admin` VALUES (1, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin', '2022-08-20 08:07:59', NULL, NULL, 1, 1660975679);
INSERT INTO `m_admin` VALUES (13, 'anita', '83349cbdac695f3943635a4fd1aaa7d0', 'kasir', NULL, 1648117026, 1, 1, 1648221384);
INSERT INTO `m_admin` VALUES (14, 'Alfi', '75c2ac5fa6e9fac7726781d01b3297b4', 'admin', NULL, 1648122863, 1, 13, 1648221432);
INSERT INTO `m_admin` VALUES (15, 'Nadya', 'f5dd238192c1f1b94c8b3110a6463249', 'admin', NULL, 1648124097, 1, 13, 1648221441);
INSERT INTO `m_admin` VALUES (19, 'usman', 'd41d8cd98f00b204e9800998ecf8427e', 'kasir', NULL, 1660960429, 1, 1, 1660960495);

-- ----------------------------
-- Table structure for m_kategori
-- ----------------------------
DROP TABLE IF EXISTS `m_kategori`;
CREATE TABLE `m_kategori`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 7 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_kategori
-- ----------------------------
INSERT INTO `m_kategori` VALUES (1, 'Elektronik', 1648133368, 1, 13, 1648221503);
INSERT INTO `m_kategori` VALUES (3, 'Tools', 1648133492, 1, 13, 1648221523);
INSERT INTO `m_kategori` VALUES (5, 'Otomotif', 1660923158, 1, NULL, NULL);
INSERT INTO `m_kategori` VALUES (6, 'Food', 1660957326, 1, 1, 1660957339);

-- ----------------------------
-- Table structure for m_pelanggan
-- ----------------------------
DROP TABLE IF EXISTS `m_pelanggan`;
CREATE TABLE `m_pelanggan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_hp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_pelanggan
-- ----------------------------
INSERT INTO `m_pelanggan` VALUES (1, 'Marion Jola', '0812324134', 'Jakarta', 1648132409, 1, 13, 1648221470);
INSERT INTO `m_pelanggan` VALUES (2, 'Dinda Maria', '08962371274', 'Malang', 1648132427, 1, 13, 1648221480);
INSERT INTO `m_pelanggan` VALUES (3, 'Nadin Hamizah', '086565', 'Jakarta\r\n', 1660909254, 18, NULL, NULL);
INSERT INTO `m_pelanggan` VALUES (4, 'Sal Priadi', '086565', 'Malang , Suhat\r\n', 1660909267, 18, 1, 1660959738);

-- ----------------------------
-- Table structure for m_produk
-- ----------------------------
DROP TABLE IF EXISTS `m_produk`;
CREATE TABLE `m_produk`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `deskripsi` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `harga` int NULL DEFAULT NULL,
  `kategori_id` int NULL DEFAULT NULL,
  `gambar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `nama`(`nama` ASC) USING BTREE,
  INDEX `kategori_id`(`kategori_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_produk
-- ----------------------------
INSERT INTO `m_produk` VALUES (1, 'Majo Pro', '<p>-</p>', 2750000, 1, 'standard_repo1.png', 1648221756, 13, 1, 1660838461);
INSERT INTO `m_produk` VALUES (2, 'majo advance', '<p>-</p>', 2750000, 1, 'paket-advance.png', 1648221796, 13, 1, 1660838492);
INSERT INTO `m_produk` VALUES (3, 'Majoo Lifestyle', '<p>-</p>', 5000000, 1, 'paket-lifestyle.png', 1648221847, 13, 1, 1660838485);
INSERT INTO `m_produk` VALUES (4, 'Majoo desktop', '<p>-</p>', 10000000, 1, 'paket-desktop.png', 1648221872, 13, 1, 1660838475);

-- ----------------------------
-- Table structure for m_supplier
-- ----------------------------
DROP TABLE IF EXISTS `m_supplier`;
CREATE TABLE `m_supplier`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `nama` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `no_hp` varchar(15) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `alamat` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of m_supplier
-- ----------------------------
INSERT INTO `m_supplier` VALUES (8, 'PT Maju Jaya', '08962371274', 'Jakarta', 1648130740, 1, 13, 1648221552);
INSERT INTO `m_supplier` VALUES (9, 'PT Violet', '089682158612', 'Malang', 1648131063, 1, 13, 1648221569);
INSERT INTO `m_supplier` VALUES (10, 'PT Baru Maju', '0865651', 'Yogyakarta', 1660918782, 1, 1, 1660918790);
INSERT INTO `m_supplier` VALUES (11, 'PT Indo Makro', '0865651', 'Malang', 1660961194, 1, NULL, NULL);

-- ----------------------------
-- Table structure for t_kartu_stok
-- ----------------------------
DROP TABLE IF EXISTS `t_kartu_stok`;
CREATE TABLE `t_kartu_stok`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `produk_id` int NULL DEFAULT NULL,
  `kode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `supplier_id` int NULL DEFAULT NULL,
  `customer_id` int NULL DEFAULT NULL,
  `catatan` text CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL,
  `reff_id` int NULL DEFAULT NULL,
  `reff_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jenis_stok` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  `harga` int NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `produk_id`(`produk_id` ASC) USING BTREE,
  INDEX `supplier_id`(`supplier_id` ASC) USING BTREE,
  INDEX `customer_id`(`customer_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 6 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_kartu_stok
-- ----------------------------
INSERT INTO `t_kartu_stok` VALUES (1, 1, 'INV18092022001', '2022-08-18', 9, NULL, 'Stok in 100', NULL, NULL, 'masuk', 100, 30000000, 1660975741, 1, NULL, NULL);
INSERT INTO `t_kartu_stok` VALUES (2, 2, 'INV18092022002', '2022-08-16', 8, NULL, 'Stock in 15', NULL, NULL, 'masuk', 50, 15000000, 1660975764, 1, NULL, NULL);
INSERT INTO `t_kartu_stok` VALUES (3, 1, 'KW2208001', '2022-08-20', NULL, 1, 'Penjualan KW2208001 | 20-08-2022', NULL, NULL, 'keluar', -3, 2750000, 1660975801, 1, NULL, NULL);
INSERT INTO `t_kartu_stok` VALUES (4, 2, 'KW2208001', '2022-08-20', NULL, 1, 'Penjualan KW2208001 | 20-08-2022', NULL, NULL, 'keluar', -1, 2750000, 1660975801, 1, NULL, NULL);
INSERT INTO `t_kartu_stok` VALUES (5, 2, 'KW2208002', '2022-08-21', NULL, 3, 'Penjualan KW2208002 | 21-08-2022', NULL, NULL, 'keluar', -1, 2750000, 1660975827, 1, NULL, NULL);

-- ----------------------------
-- Table structure for t_penjualan
-- ----------------------------
DROP TABLE IF EXISTS `t_penjualan`;
CREATE TABLE `t_penjualan`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `kode` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci NULL DEFAULT NULL,
  `tanggal` date NULL DEFAULT NULL,
  `pelanggan_id` int NULL DEFAULT NULL,
  `admin_id` int NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `pelanggan_id`(`pelanggan_id` ASC) USING BTREE,
  INDEX `admin_id`(`admin_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_penjualan
-- ----------------------------
INSERT INTO `t_penjualan` VALUES (1, 'KW2208001', '2022-08-20', 1, 1, 1660975801, 1, NULL, NULL);
INSERT INTO `t_penjualan` VALUES (2, 'KW2208002', '2022-08-21', 3, 1, 1660975827, 1, NULL, NULL);

-- ----------------------------
-- Table structure for t_penjualan_det
-- ----------------------------
DROP TABLE IF EXISTS `t_penjualan_det`;
CREATE TABLE `t_penjualan_det`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `penjualan_id` int NULL DEFAULT NULL,
  `produk_id` int NULL DEFAULT NULL,
  `harga` int NULL DEFAULT NULL,
  `jumlah` int NULL DEFAULT NULL,
  `total` int NULL DEFAULT NULL,
  `created_at` int NULL DEFAULT NULL,
  `created_by` int NULL DEFAULT NULL,
  `updated_at` int NULL DEFAULT NULL,
  `updated_by` int NULL DEFAULT NULL,
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `penjualan_id`(`penjualan_id` ASC) USING BTREE,
  INDEX `produk_id`(`produk_id` ASC) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of t_penjualan_det
-- ----------------------------
INSERT INTO `t_penjualan_det` VALUES (1, 1, 1, 2750000, 3, 8250000, 1660975801, 1, NULL, NULL);
INSERT INTO `t_penjualan_det` VALUES (2, 1, 2, 2750000, 1, 2750000, 1660975801, 1, NULL, NULL);
INSERT INTO `t_penjualan_det` VALUES (3, 2, 2, 2750000, 1, 2750000, 1660975827, 1, NULL, NULL);

SET FOREIGN_KEY_CHECKS = 1;
