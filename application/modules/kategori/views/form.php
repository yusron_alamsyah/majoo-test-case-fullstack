<div class="modal-dialog modal-md" style="width: 100%;" role="document">
    <div class="modal-content">
        <div class="modal-header overview-item--c2">
            <center>
                <h5 style="color: white;" class="modal-title" id="title-tambah">Form Kategori</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
        <form method="post" id="form-add" onsubmit="return ajax_action_add();">
            <input type="text" style="display: none;" id="id" name="id">
            <div class="modal-body">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <label class="required">Nama :</label>
                            <input type="text" name="nama" id="nama" class="form-control">
                        </td>
                    </tr>
                </table>
            </div>
            <div class="modal-footer">
                <a><button type="button" data-dismiss="modal" class="btn btn-danger">Back</button></a>
                <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
            </div>
        </form>
    </div>
</div>