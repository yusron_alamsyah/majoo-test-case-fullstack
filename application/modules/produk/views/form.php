<div class="modal-dialog modal-lg" style="width: 100%;" role="document">
    <div class="modal-content">
        <div class="modal-header overview-item--c2">
            <center>
                <h4 style="color: white;" class="modal-title" id="title-tambah">Form Produk</h4>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
            <form method="post" id="form-add" enctype="multipart/form-data">
                <input type="text" style="display: none;" id="id" name="id">
                <div class="modal-body">
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <label>Nama :</label>
                                    <input type="text" name="nama" id="nama" class="form-control">
                                </td>
                                <td style="width:50%;" rowspan="3">
                                    <label>Gambar :</label>
                                    <input type="file" name="gambar" id="gambar" class="form-control" data-allowed-file-extensions='["jpg", "png","jpeg"]'>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Harga :</label>
                                    <input type="text" name="harga" id="harga" class="form-control">
                                </td>
                            </tr>
                                <td>
                                    <label>Kategori :</label>
                                    <select class="form-control select2" style="width: 100%;" name="kategori_id" id="kategori_id">
                                    <option value="">Pilihan</option>
                                    <?php foreach ($list_kategori as $value) { ?>
                                      <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                    <?php } ?>
                                  </select>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <label>Deskripsi :</label>
                                   <textarea name="deskripsi" id="editor" rows="10" cols="80" class="ckeditor">
                                        
                                    </textarea>
                                </td>

                            </tr>
                        </table>
                </div>
                <div class="modal-footer">
                    <a ><button type="button" data-dismiss="modal"  onclick="reset_form()" class="btn btn-danger">Back</button></a>
                    <button onclick="return ajax_action_add();" type="button" id="btn_admin" class="btn btn-success">Save</button>
                </div>
        </form>
    </div>
</div>


