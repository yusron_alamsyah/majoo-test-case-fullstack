
<button onclick="reset_form()" data-toggle="modal" data-target="#modal_add"  class="btn pull-right btn-primary btn-sm">
    <i class="fas fa-plus"></i>
        New Produk
</button>
<div class="mb-1"><h4 class="text-title">Produk</h4></div>
<br><br>
<table class="table table-borderless table-striped " id="list_data">
    <thead style="background: #000; color: #FFF;">
        <tr>
            <th>
                <center>No</center>
            </th>
            <th>
                <center>Nama</center>
            </th>
            <th>
                <center>Harga</center>
            </th>
            <th>
                <center>Kategori</center>
            </th>
            <th>
                <center>Stok</center>
            </th>
            <th>
                <center>Action</center>
            </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>



<script type="text/javascript">
var base_url = '<?php echo base_url('uploads'); ?>';
function ajax_list() {

        $('#list_data').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url(); ?>produk/ajax_list/",
                "type": "POST",
                "data": {
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                }

            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            }, ],

        });
}
ajax_list();

function reload_list(){
    $('#list_data').DataTable().ajax.reload();
}

function reset_form(){
    $(".info-password").addClass("d-none");

    document.getElementById("form-add").reset();
}

function ajax_get_edit(id){
    $.ajax({
            url: "<?php echo base_url(); ?>produk/ajax_get_edit/",
            type: 'GET',
            dataType: "json",
            data: {
                id: id
            },
            beforeSend: function() {
                $('#page-load').show();
                reset_form();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    var list = data.message.body;

                    $('#modal_add').modal("toggle");
                    $("#nama").val(list.nama);
                    $("#harga").val(list.harga);
                    $("#kategori_id").val(list.kategori_id).select2();
                    $("#id").val(list.id);
                    editor.setData((list.deskripsi == null ? '' : list.deskripsi));


                    if (list.gambar != null) {
                        $("#gambar").fileinput('destroy');
                        $("#gambar").fileinput({
                            'required':true,
                            'showUpload':false, 
                            'showRemove':false, 
                            'previewFileType':'image',
                            'initialPreviewAsData': true,
                            'initialPreviewFileType': 'image',
                            'initialPreview': "./uploads/produk/"+list.gambar,
                            'allowedFileTypes':['image'],
                            'removeLabel': ''
                        });
                    }else{
                        $("#gambar").fileinput('destroy');
                        $("#gambar").fileinput({'showUpload':false, 'previewFileType':'image','required':true,'allowedFileTypes':['image']});
                    }
                    
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
}



function ajax_action_add() {
    var form = $('#form-add')[0];
    var data = new FormData(form);
    var deskripsi = editor.getData();
    data.append('deskripsi',deskripsi)
    data.append('<?php echo $this->security->get_csrf_token_name(); ?>',"<?php echo $this->security->get_csrf_hash(); ?>");

    $.ajax({
        url: "<?php echo base_url() ?>produk/ajax_action_add/",
        type: 'POST',
        dataType: "json",
        data:data,
        enctype: 'multipart/form-data',
        processData: false,  
        contentType: false,
        cache: false,
        beforeSend: function() {
            $('#page-load').show();
        },
        success: function(data) {
            $('#page-load').hide();
            if (data.result) {
                
                toastr["success"](data.message.body);
                $('#modal_add').modal("toggle");
                reload_list();

            } else {
                toastr["error"](data.message.body);
            }

        },
        error: function(request, status, error) {
            $('#page-load').hide();
            toastr["error"]("Error, Please try again later");
        }
    });

    return false;
}

function ajax_action_delete(id) {
    if (confirm('Are You sure delete this data?')) {
        $.ajax({
            url: "<?php echo base_url(); ?>produk/ajax_action_delete/",
            type: 'POST',
            dataType: "json",
            data: {
                id: id,
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    toastr["success"](data.message.body);
                    reload_list();
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
    }
}

function reset_form(){
    $('#form-add')[0].reset();
    $('select').val('').select2()
    editor.setData('');

    $("#gambar").fileinput('destroy');
    $("#gambar").fileinput({'showUpload':false, 'previewFileType':'image','required':true,'allowedFileTypes':['image']});
}
</script>