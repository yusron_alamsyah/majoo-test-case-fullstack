<?php defined('BASEPATH') or exit('No direct script access allowed');

class Produk extends MY_Controller
{

    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();

        $this->load->model("M_produk");
        $this->load->library('session');
        $this->load->library(array('session', 'pagination', 'form_validation'));
        $this->load->helper(array('form', 'url'));
    }

    public function index()
    {
        cekLogin();
        $data['content'] = 'produk';
        $data['form']    = 'form';
        $data["list_kategori"] = $this->M_produk->fetch_table("*", "m_kategori","");
        $this->load->view('login/home_page.php', $data);

    }

    public function list_produk()
    {

        $data["produk"] = $this->M_produk->fetch_table("*", "m_produk","");
        $this->load->view('list_produk.php', $data);

    }

    public function ajax_list()
    {
        $column        = 'm_produk.*,m_kategori.nama as nama_ketegori';
        $columnOrder  = array(null, 'm_produk.nama', 'harga', 'm_kategori.nama', null); //set column field database for datatable orderable
        $columnSearch = array('m_produk.nama', 'harga', 'm_kategori.nama'); //set column field database for datatable searchable
        $order         = array('id' => 'DESC'); // default order
        $table         = "m_produk";
        $where         = "";
        $joins         = array(
                            array(
                                'table' => 'm_kategori',
                                'condition' => 'm_kategori.id = m_produk.kategori_id',
                                'jointype' => ''
                            )
                        );

        $list = $this->M_produk->get_datatables($column, $table, $columnOrder, $columnSearch, $order, $where, $joins);

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $key) {
            $getStok =  $this->M_produk->fetch_table("sum(jumlah) as stok", "t_kartu_stok","produk_id = '".$key->id."' ");

            $sisaStok = isset($getStok[0]->stok) ? $getStok[0]->stok : 0;

            $no++;
            $row   = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = $key->nama;
            $row[] = "<div class='text-right'>".number_format($key->harga)."</div>";
            $row[] = $key->nama_ketegori;
            $row[] = $sisaStok;
            $row[] = "
            <center><button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit(" . $key->id . ")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                    <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete(" . $key->id . ")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button></center>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->M_produk->count_all($table, $where, $joins),
            "recordsFiltered" => $this->M_produk->count_filtered($column, $table, $columnOrder, $columnSearch, $order, $where, $joins),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_edit()
    {

        $id  = get("id");
        $cek = $this->M_produk->fetch_table("*", "m_produk", "id = '" . get('id') . "'");
        if ($cek) {
            return successResponse($cek[0]);
        } else {
            return unprocessResponse('Failed to get data');
        }
    }
    public function ajax_action_add()
    {
        $id = post("id");

        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required|numeric');
        $this->form_validation->set_rules('kategori_id', 'kategori', 'required');
        $this->form_validation->set_rules('deskripsi', 'deskripsi', 'required');
        
        if (empty($id)) {
            if (empty($_FILES['gambar']['name']))
            {
                $this->form_validation->set_rules('gambar', 'gambar', 'required');
            }
        }
        
        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();

            return unprocessResponse(displayError($error), $error, '');
        } else {

            // cek_duplicate
            if (empty($id)) {
                $cek = $this->M_produk->fetch_table("id", "m_produk", "nama = '" . post('nama') . "'");
            }else{
                $cek = $this->M_produk->fetch_table("id", "m_produk", "nama = '" . post('username') . "' and id != '".$id."' ");
            }
            if (count($cek) > 0) {
                return unprocessResponse('Duplicate Product '.post("nama"));
            }

            $data = array(
                "nama" => post("nama"),
                "harga"     => post("harga"),
                "kategori_id"     => post("kategori_id"),
                "deskripsi" => post("deskripsi"),
                
            );

            $config['upload_path']          = './uploads/produk';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 1024;

            $this->load->library('upload', $config);
            if ( ! $this->upload->do_upload('gambar'))
            {
                $error = array('error' => $this->upload->display_errors());
                // echo json_encode($_FILES['gambar']); die();
                // return unprocessResponse($this->upload->display_errors());
            }
            else
            {
                $fileGambar = array('upload_data' => $this->upload->data());
                
                $data['gambar'] = $fileGambar['upload_data']['file_name'];

                if (isset($id) && !empty($id)) {
                    $this->delete_image($id);
                }
                
            }

            if (isset($id) && !empty($id)) {
                $add = $this->M_produk->update_table("m_produk", $data,"id",$id);   
            }else{
                $add = $this->M_produk->insert_table("m_produk", $data);   
            }
            if ($add == false) {
                return unprocessResponse('Failed to save data');
            } else {
                return successResponse('Success Save', '' . base_url() . 'user/');
            }

        }

    }

    public function ajax_action_delete()
    {
        $delete = $this->M_produk->delete_table("m_produk", "id", post("id"));
        if ($delete == false) {
            return unprocessResponse('Failed to delete data');
        } else {
            return successResponse('Success Delete', '' . base_url() . 'user/');
        }
    }

    public function delete_image($id='')
    {
        $cek = $this->M_produk->fetch_table("*", "m_produk", "id = '" . $id . "'");

        if ($cek && $cek[0]->gambar != null) {
                unlink('./uploads/produk/'.$cek[0]->gambar);
                return true;
        }else{
            return false;
        }
    }

}