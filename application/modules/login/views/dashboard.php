<div class="row m-t-25">
    <div class="col-sm-6 col-lg-6">
        <div class="overview-item overview-item--c2">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-shopping-cart"></i>
                    </div>
                    <div class="text">
                        <h2> <?php echo $order_monthly; ?></h2>
                        <span>completed order</span><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-6 col-lg-6">
        <div class="overview-item overview-item--c4">
            <div class="overview__inner">
                <div class="overview-box clearfix">
                    <div class="icon">
                        <i class="zmdi zmdi-account-o"></i>
                    </div>
                    <div class="text">
                        <h2><?php echo $active_pelanggan; ?></h2>
                        <span>total pelanggan</span><br><br>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>