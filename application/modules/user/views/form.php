<div class="modal-dialog modal-lg" style="width: 100%;" role="document">
    <div class="modal-content">
        <div class="modal-header overview-item--c2">
            <center>
                <h4 style="color: white;" class="modal-title" id="title-tambah">Form User</h4>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
            <form method="post" id="form-add" onsubmit="return ajax_action_add();">
                <input type="text" style="display: none;" id="id" name="id">
                <div class="modal-body">
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <label>Username :</label>
                                    <input type="text" name="username" id="username" class="form-control">
                                </td>
                                <td rowspan="2">
                                    <label>Password :</label>
                                    <input type="password" name="password" id="password" class="form-control">
                                    <small class="d-none info-password">Kosongi Password jika tidak diubah</small>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <label>Role :</label>
                                    <select name="role" class="form-control" id="role">
                                        <option value=""></option>
                                        <option value="admin">Admin</option>
                                        <option value="kasir">Kasir</option>
                                    </select>
                                </td>

                            </tr>
                        </table>
                </div>
                <div class="modal-footer">
                    <a ><button type="button" data-dismiss="modal" class="btn btn-danger">Back</button></a>
                    <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
                </div>
        </form>
    </div>
</div>