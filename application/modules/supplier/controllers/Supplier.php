<?php defined('BASEPATH') or exit('No direct script access allowed');

class Supplier extends MY_Controller
{
    private $_table = 'm_supplier';
    private $_primaryKey  = 'id';

    private $_content = 'supplier';
    private $_form = 'form';

    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();

        $this->load->helper('url');
        $this->load->model("M_supplier");
        $this->load->library('session');
        $this->load->library(array('session', 'pagination', 'form_validation'));
    }

    public function index()
    {
        cekLogin();
        $data['content'] = $this->_content;
        $data['form']    = $this->_form;
        $this->load->view('login/home_page.php', $data);

    }

    public function ajax_list()
    {
        $column        = '*';
        $columnOrder  = array(null, 'nama', 'no_hp', 'alamat', null); //set column field database for datatable orderable
        $columnSearch = array('nama', 'no_hp', 'alamat'); //set column field database for datatable searchable
        $order         = array($this->_primaryKey => 'DESC'); // default order

        $list = $this->M_supplier->get_datatables($column, $this->_table, $columnOrder, $columnSearch, $order);

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row   = array();
            $row[] = '<center>'.$no.'</center>';
            $row[] = $key->nama;
            $row[] = $key->no_hp;
            $row[] = $key->alamat;
            $row[] = "
            <center><button data-toggle='tooltip' data-placement='top' title='Edit' onclick='ajax_get_edit(" . $key->id . ")' class='btn btn-success btn-sm'><i class='zmdi zmdi-edit'></i></button>
                    <button data-toggle='tooltip' data-placement='top' title='Delete' onclick='ajax_action_delete(" . $key->id . ")' class='btn btn-danger btn-sm'><i class='zmdi zmdi-delete'></i></button></center>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->M_supplier->count_all($this->_table),
            "recordsFiltered" => $this->M_supplier->count_filtered($column, $this->_table, $columnOrder, $columnSearch, $order),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_edit()
    {

        $id  = get("id");
        $cek = $this->M_supplier->fetch_table("*", $this->_table, $this->_primaryKey." = '" . get('id') . "'");
        if ($cek) {
            return successResponse($cek[0]);
        } else {
            return unprocessResponse('Failed to get data');
        }
    }
    public function ajax_action_add()
    {

        $this->form_validation->set_rules('nama', 'nama', 'required');
        $this->form_validation->set_rules('no_hp', 'no_hp', 'required|numeric');
        $this->form_validation->set_rules('alamat', 'alamat', 'required');

        $id = post("id");

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();

            return unprocessResponse(displayError($error), $error, '');
        } else {


            $data = array(
                "nama" => post("nama"),
                "no_hp"     => post("no_hp"),
                "alamat" => post("alamat")
            );
            if (isset($id) && !empty($id)) {
                $add = $this->M_supplier->update_table($this->_table, $data,$this->_primaryKey,$id);   
            }else{
                $add = $this->M_supplier->insert_table($this->_table, $data);   
            }
            if ($add == false) {
                return unprocessResponse('Failed to save data');
            } else {
                return successResponse('Success Save', '' . base_url() . 'user/');
            }

        }

    }

    public function ajax_action_delete()
    {
        $delete = $this->M_supplier->delete_table($this->_table, $this->_primaryKey, post("id"));
        if ($delete == false) {
            return unprocessResponse('Failed to delete data');
        } else {
            return successResponse('Success Delete', '' . base_url() . 'user/');
        }
    }

}
