<?php defined('BASEPATH') or exit('No direct script access allowed');

class Reports extends MY_Controller
{

    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();

        $this->load->helper('url');
        $this->load->model("M_Reports");
        $this->load->library('session');
        $this->load->library(array('session', 'pagination', 'form_validation'));
    }

    public function sales()
    {
        cekLogin();
        $data['content'] = 'reports_sales';
        $this->load->view('login/home_page.php', $data);
    }

    public function sales_produk()
    {
        cekLogin();
        $data['content'] = 'reports_sales_product';
        $data["list_product"] = $this->M_Reports->fetch_table("*", "m_produk", "");
        $this->load->view('login/home_page.php', $data);
    }

    public function receipt_item()
    {
        cekLogin();
        $data['content'] = 'receipt_item';
        $data["list_supplier"] = $this->M_Reports->fetch_table("*", "m_supplier", "");
        $this->load->view('login/home_page.php', $data);
    }

    public function stok()
    {
        cekLogin();
        $data['content'] = 'reports_stock';
        $data["list_product"] = $this->M_Reports->fetch_table("*", "m_produk", "");
        $this->load->view('login/home_page.php', $data);
    }

    public function ajax_show_sales()
    {

        $joins = array(
            array(
                'table'     => 'm_pelanggan',
                'condition' => 'm_pelanggan.id = t_penjualan.pelanggan_id',
                'jointype'  => '',
            ),
        );
        $where = "t_penjualan.id > 0";
        $tanggal = get("tanggal");
        $kode = get("kode");
        $filter = "";
        if (!empty($tanggal)) {
            $where .= " and t_penjualan.tanggal = '" . date("Y-m-d", strtotime(get("tanggal"))) . "'";
            $filter .= " Periode " . date("d-m-Y", strtotime(get("tanggal")));
        }
        if (!empty($kode)) {
            $where .= " and t_penjualan.kode LIKE '%" . get("kode") . "%' ";
            $filter .= " Kode " . $kode;
        }
        $list = $this->M_Reports->fetch_joins("t_penjualan.*,m_pelanggan.nama", "t_penjualan", $joins, $where);

        foreach ($list as $key => $value) {
            $getTotal = $this->M_Reports->fetch_table("sum(total) as total", "t_penjualan_det", "t_penjualan_det.penjualan_id = '" . $value->id . "' ");
            $value->tanggal = date("d-m-Y", strtotime($value->tanggal));
            $value->total = isset($getTotal[0]->total) ? number_format($getTotal[0]->total) : 0;
        }

        $return = array(
            "list" => $list,
            "filter" => empty($filter) ? "Filter All" : $filter,
        );
        return successResponse($return, '');
    }

    public function ajax_show_sales_product()
    {

        $joins = array(
            array(
                'table'     => 't_penjualan_det',
                'condition' => 't_penjualan.id = t_penjualan_det.penjualan_id',
                'jointype'  => '',
            ),
            array(
                'table'     => 'm_produk',
                'condition' => 'm_produk.id = t_penjualan_det.produk_id',
                'jointype'  => '',
            )
        );
        $where = "t_penjualan.id > 0";
        $tanggal = get("tanggal");
        $produkId = get("produk_id");
        $filter = "";
        if (!empty($tanggal)) {
            $where .= " and t_penjualan.tanggal = '" . date("Y-m-d", strtotime(get("tanggal"))) . "'";
            $filter .= " Periode " . date("d-m-Y", strtotime(get("tanggal")));
        }
        if (!empty($produkId)) {
            $where .= " and t_penjualan_det.produk_id = '" . get("produk_id") . "' ";
        }
        $list = $this->M_Reports->fetch_joins("t_penjualan_det.*,t_penjualan.kode,t_penjualan.tanggal,m_produk.nama as nama_produk,t_penjualan_det.produk_id", "t_penjualan", $joins, $where);

        $arrProduk = [];
        $cekFilterProduk = false;
        foreach ($list as $key => $value) {
            if (!empty($produkId) && $cekFilterProduk == false) {
                $filter .= " Product " . $value->nama_produk;
                $cekFilterProduk = true;
            }
            $value->tanggal = date("d-m-Y", strtotime($value->tanggal));

            $arrProduk[$value->produk_id]["nama_produk"] = $value->nama_produk;
            $value->harga = number_format($value->harga);
            $value->total = number_format($value->total);
            $arrProduk[$value->produk_id]["detail"][] = $value;
        }

        $return = array(
            "list" => $arrProduk,
            "filter" => empty($filter) ? "Filter All" : $filter,
        );
        return successResponse($return, '');
    }


    public function ajax_show_kartu_stok()
    {

        $joins = array(
            array(
                'table'     => 'm_supplier',
                'condition' => 'm_supplier.id = t_kartu_stok.supplier_id',
                'jointype'  => 'left',
            ),
            array(
                'table'     => 'm_pelanggan',
                'condition' => 'm_pelanggan.id = t_kartu_stok.customer_id',
                'jointype'  => 'left',
            ),
            array(
                'table'     => 'm_produk',
                'condition' => 'm_produk.id = t_kartu_stok.produk_id',
                'jointype'  => '',
            )
        );
        $where = "t_kartu_stok.id > 0";
        $produkId = get("produk_id");
        $filter = "";
        if (!empty($produkId)) {
            $where .= " and t_kartu_stok.produk_id = '" . get("produk_id") . "' ";
        }
        $list = $this->M_Reports->fetch_joins("t_kartu_stok.*,m_pelanggan.nama as nama_customer,m_supplier.nama as nama_supplier,m_produk.nama as nama_produk", "t_kartu_stok", $joins, $where);

        $arrProduk = [];
        $cekFilterProduk = false;
        foreach ($list as $key => $value) {
            if (!empty($produkId) && $cekFilterProduk == false) {
                $filter .= " Product " . $value->nama_produk;
                $cekFilterProduk = true;
            }

            $value->tanggal = date("d-m-Y", strtotime($value->tanggal));

            $arrProduk[$value->produk_id]["nama_produk"] = $value->nama_produk;
            if (!isset($arrProduk[$value->produk_id]["stok"])) {
                $arrProduk[$value->produk_id]["stok"] = 0;
            }
            if (isset($arrProduk[$value->produk_id]["stok"])) {
                if ($value->jenis_stok == "masuk") {
                    $value->nama_reff = $value->nama_supplier;
                    $value->class = "badge badge-success";
                    $arrProduk[$value->produk_id]["stok"] += $value->jumlah;
                } else {
                    $value->nama_reff = $value->nama_customer;
                    $value->class = "badge badge-danger";
                    $arrProduk[$value->produk_id]["stok"] -= abs($value->jumlah);
                }
            }


            $arrProduk[$value->produk_id]["detail"][] = $value;
        }

        $return = array(
            "list" => $arrProduk,
            "filter" => empty($filter) ? "Filter All" : $filter,
        );
        return successResponse($return, '');
    }


    public function ajax_show_pembelian()
    {

        $joins = array(
            array(
                'table'     => 'm_supplier',
                'condition' => 'm_supplier.id = t_kartu_stok.supplier_id',
                'jointype'  => '',
            ),
            array(
                'table'     => 'm_produk',
                'condition' => 'm_produk.id = t_kartu_stok.produk_id',
                'jointype'  => '',
            )
        );
        $where = "t_kartu_stok.jenis_stok = 'masuk'";
        $tanggal = get("tanggal");
        $supplierId = get("supplier_id");
        $filter = "";
        if (!empty($tanggal)) {
            $where .= " and t_kartu_stok.tanggal = '" . date("Y-m-d", strtotime(get("tanggal"))) . "'";
            $filter .= " Periode " . date("d-m-Y", strtotime(get("tanggal")));
        }
        if (!empty($supplierId)) {
            $where .= " and t_kartu_stok.supplier_id = '" . get("supplier_id") . "' ";
        }
        $list = $this->M_Reports->fetch_joins("t_kartu_stok.*,m_supplier.nama as nama_supplier,m_produk.nama as nama_produk", "t_kartu_stok", $joins, $where);

        foreach ($list as $key => $value) {
            $value->tanggal = date("d-m-Y", strtotime($value->tanggal));
        }

        $return = array(
            "list" => $list,
            "filter" => empty($filter) ? "Filter All" : $filter,
        );
        return successResponse($return, '');
    }
}
