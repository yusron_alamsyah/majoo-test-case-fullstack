<div class="mb-4"><h4 class="text-title">Reports Stock</h4></div>
<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body" style="background:#fafafa">
                <form id="form-laporan">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Product</label>
                                <div class="col-sm-8">
                                        <select class="form-control select2" style="width: 100%;" name="produk_id" id="produk_id">
                                                <option value="">Pilihan</option>
                                                <?php foreach ($list_product as $value) { ?>
                                                  <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                                <?php } ?>
                                        </select>
                                </div>
                              </div>
                        </td>
                        <!-- <td>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Tanggal</label>
                                <div class="col-sm-8">
                                  <input type="text"  class="form-control form-control-sm datetime" id="tanggal" name="tanggal">
                                  <small>*Kosongi untuk memilih Semua periode</small>

                                </div>
                              </div>
                        </td> -->
                    </tr>
                </table>
                </form>
                <center>
                <div class="btn btn-group">
                    <button onclick="reset_laporan()" class="btn btn-sm btn-danger"><i class="fa fa-refresh"></i> Reset</button>    
                    <button onclick="ajax_show_sales()" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Show</button>
                </div>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row" id="showReport" style="display:none;">
        <div class="col-md-2">
            <img src="https://majoo.id/assets/img/main-logo.png" style="width:100%;" alt="Majoo" />
        </div>
        <div class="col-md-6 pr-4">
            <h3>Report Stock</h3>          
            <small class="filter"><b>All Periode</b></small>
        </div>
        <div class="col-md-12">
            <hr>
            <table class="table table-striped">
                <tr class="bg-dark text-white">
                    <!-- <td>No</td> -->
                    <td>Kode</td>
                    <td>Tanggal</td>
                    <td>Catatan</td>
                    <td>Type</td>
                    <td>Jumlah</td>
                </tr>
                <tbody id="body-laporan">
                    
                </tbody>
            </table>
        </div>
    </div>




<script type="text/javascript">


function reset_laporan(){
    document.getElementById("form-laporan").reset();
    // $("#produk_id").select2("val", "");
    $('#produk_id').val('').trigger('change');
    $("#showReport").hide();
}

function ajax_show_sales(){
    $.ajax({
            url: "<?php echo base_url(); ?>reports/ajax_show_kartu_stok/",
            type: 'GET',
            dataType: "json",
            data: $("#form-laporan").serialize(),
            beforeSend: function() {
                $('#page-load').show();
                 $("#body-laporan").html("");
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    $(".filter").html(data.message.body.filter);
                    var list = data.message.body.list;
                    var tr = "";  var jumlah = 0;
                    for (var key in list) {
                        var value = list[key];
                        var tr = "<tr>";
                        tr = tr + "<td class='bg-success text-white' colspan='4'>"+value.nama_produk+"</td>";
                        tr = tr + "<td class='bg-success text-white'><strong class='text-right'>"+value.stok+"</strong></td>";
                        tr = tr + "</tr>";
                        
                        var detail = value.detail;
                        var no = 1;
                        for(var key_det in detail){
                            var val_det = detail[key_det];
                            tr = tr + "<tr>";
                            // tr = tr + "<td>"+no+"</td>";
                            tr = tr + "<td>"+val_det.kode+"</td>";
                            tr = tr + "<td>"+val_det.tanggal+"</td>";
                            tr = tr + "<td>From : "+
                                    val_det.nama_reff+"<br><small>Catatan : "+val_det.catatan+
                                    "</small></td>";
                            tr = tr + "<td><span class='"+val_det.class+"'>"+val_det.jenis_stok+"</span></td>";
                            tr = tr + "<td>"+val_det.jumlah+"</td>";
                            tr = tr + "</tr>";
                            
                            no = parseInt(no) +1;
                        }
                       
                        jumlah = parseInt(jumlah) +1;
                        $("#body-laporan").append(tr);

                    };

                    if (jumlah == 0) {
                       $("#body-laporan").append("<tr><td colspan='5'><center>Data Kosong</cente></td></tr>");
                    }
                    $("#showReport").show();
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
}

</script>