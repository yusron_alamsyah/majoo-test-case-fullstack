<div class="mb-4"><h4 class="text-title">Reports Sales</h4></div>
<div class="row">
    <div class="col-md-12 mb-4">
        <div class="card">
            <div class="card-body" style="background:#fafafa">
                <form id="form-laporan">
                <table class="table table-borderless">
                    <tr>
                        <td>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Kode</label>
                                <div class="col-sm-8">
                                  <input type="text"  class="form-control form-control-sm" id="kode" name="kode">
                                </div>
                              </div>
                        </td>
                        <td>
                            <div class="form-group row">
                                <label class="col-sm-4 col-form-label">Tanggal</label>
                                <div class="col-sm-8">
                                  <input type="text"  class="form-control form-control-sm datetime" id="tanggal" name="tanggal">
                                  <small>*Kosongi untuk memilih Semua periode</small>

                                </div>
                              </div>
                        </td>
                    </tr>
                </table>
                </form>
                <center>
                <div class="btn btn-group">
                    <button onclick="reset_laporan()" class="btn btn-sm btn-danger"><i class="fa fa-refresh"></i> Reset</button>    
                    <button onclick="ajax_show_sales()" class="btn btn-sm btn-primary"><i class="fa fa-eye"></i> Show</button>
                </div>
                </center>
            </div>
        </div>
    </div>
</div>
<div class="row" id="showReport" style="display:none;">
        <div class="col-md-2">
            <img src="https://majoo.id/assets/img/main-logo.png" style="width:100%;" alt="Majoo" />
        </div>
        <div class="col-md-6 pr-4">
            <h3>Report Sales</h3>          
            <small class="filter"><b>All Periode</b></small>
        </div>
        <div class="col-md-12">
            <hr>
            <table class="table table-striped">
                <tr class="bg-dark text-white">
                    <td>No</td>
                    <td>Kode</td>
                    <td>Tanggal</td>
                    <td>Customer</td>
                    <td>Total</td>
                </tr>
                <tbody id="body-laporan">
                    
                </tbody>
            </table>
        </div>
    </div>




<script type="text/javascript">


function reset_laporan(){
    document.getElementById("form-laporan").reset();
    $("#showReport").hide();
}

function ajax_show_sales(){
    $.ajax({
            url: "<?php echo base_url(); ?>reports/ajax_show_sales/",
            type: 'GET',
            dataType: "json",
            data: $("#form-laporan").serialize(),
            beforeSend: function() {
                $('#page-load').show();
                 $("#body-laporan").html("");
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    $(".filter").html(data.message.body.filter);
                    var list = data.message.body.list;
                    var tr = ""; var no = 1; var jumlah = 0;
                    for (var key in list) {
                        var value = list[key];
                        var tr = "<tr>";
                        tr = tr + "<td>"+no+"</td>";
                        tr = tr + "<td>"+value.kode+"</td>";
                        tr = tr + "<td>"+value.tanggal+"</td>";
                        tr = tr + "<td>"+value.nama+"</td>";
                        tr = tr + "<td class='text-right'>"+value.total+"</td>";
                        tr = tr + "</tr>";

                        no = parseInt(no) +1;
                        jumlah = parseInt(jumlah) +1;
                        $("#body-laporan").append(tr);

                    };

                    if (jumlah == 0) {
                       $("#body-laporan").append("<tr><td colspan='5'><center>Data Kosong</cente></td></tr>");
                    }
                    $("#showReport").show();
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
}

</script>