<div class="modal-dialog modal-lg" style="width: 100%;" role="document">
    <div class="modal-content">
        <div class="modal-header overview-item--c2">
            <center>
                <h5 style="color: white;" class="modal-title" id="title-tambah">Form Receipt Item</h5>
            </center>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">×</span>
            </button>
        </div>
            <form method="post" id="form-add" onsubmit="return ajax_action_add();">
                <input type="text" style="display: none;" id="id" name="id">
                <div class="modal-body">
                        <table class="table table-borderless">
                            <tr>
                                <td>
                                    <div class="form-group">
                                        <label class="required">No.Invoice :</label>
                                        <input type="text" name="no_invoice" id="no_invoice" class="form-control">
                                    </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                        <label class="required">Tanggal :</label>
                                        <input type="text" class="form-control datetime" id="tanggal" name="tanggal">
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                            <label class="required">Supplier :</label>
                                            <select class="form-control select2" style="width: 100%;" name="supplier_id" id="supplier_id">
                                                <option value="">Pilihan</option>
                                                <?php foreach ($list_supplier as $value) { ?>
                                                  <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                                <?php } ?>
                                              </select>
                                        </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                            <label class="required">Product :</label>
                                            <select class="form-control select2" style="width: 100%;" name="produk_id" id="produk_id">
                                                <option value="">Pilihan</option>
                                                <?php foreach ($list_product as $value) { ?>
                                                  <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?></option>
                                                <?php } ?>
                                              </select>
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <div class="form-group">
                                            <label class="required">Jumlah :</label>
                                            <input type="number" name="jumlah" id="jumlah" class="form-control">
                                        </div>
                                </td>
                                <td>
                                    <div class="form-group">
                                            <label class="required">Harga :</label>
                                            <input type="number" name="harga" id="harga" class="form-control">
                                        </div>
                                </td>
                            </tr>
                            <tr>
                                <td colspan="2">
                                    <div class="form-group">
                                        <label class="required">Catatan :</label>
                                        <textarea name="catatan" id="catatan" class="form-control"></textarea>
                                    </div>
                                </td>
                            </tr>
                        </table>
                </div>
                <div class="modal-footer">
                    <a ><button type="button" data-dismiss="modal" class="btn btn-danger">Back</button></a>
                    <button type="submit" id="btn_admin" class="btn btn-success">Save</button>
                </div>
        </form>
    </div>
</div>
