<?php defined('BASEPATH') or exit('No direct script access allowed');

class Penerimaan_barang extends MY_Controller
{

    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();

        $this->load->helper('url');
        $this->load->model("M_penerimaan_barang");
        $this->load->library('session');
        $this->load->library(array('session', 'pagination', 'form_validation'));
    }

    public function index()
    {
        cekLogin();
        $data['content'] = 'penerimaan_barang';
        $data['form']    = 'form';
        $data["list_supplier"] = $this->M_penerimaan_barang->fetch_table("*", "m_supplier","");
        $data["list_product"] = $this->M_penerimaan_barang->fetch_table("*", "m_produk","");
        $this->load->view('login/home_page.php', $data);

    }

    public function ajax_list()
    {
        $column        = 't_kartu_stok.*,m_produk.nama as nama_produk,m_supplier.nama as nama_supplier';
        $columnOrder  = array(null, 'no_invoice', "m_supplier.nama","m_produk.nama","jumlah","harga","tanggal"); //set column field database for datatable orderable
        $columnSearch = array('m_produk.nama',"m_supplier.nama","no_invoice"); //set column field database for datatable searchable
        $order         = array('t_kartu_stok.id' => 'DESC'); // default order
        $table         = "t_kartu_stok";
        $where         = "";
        $joins         = array(
                            array(
                                'table' => 'm_produk',
                                'condition' => 'm_produk.id = t_kartu_stok.produk_id',
                                'jointype' => ''
                            ),
                            array(
                                'table' => 'm_supplier',
                                'condition' => 'm_supplier.id = t_kartu_stok.supplier_id',
                                'jointype' => ''
                            )
                        );

        $list = $this->M_penerimaan_barang->get_datatables($column, $table, $columnOrder, $columnSearch, $order, "t_kartu_stok.jenis_stok = 'masuk' ", $joins);

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row   = array();
            $row[] = '<center>'.$key->kode.'</center>';
            $row[] = $key->nama_supplier;
            $row[] = $key->nama_produk;
            $row[] = $key->jumlah;
            $row[] = $key->catatan;
            
            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->M_penerimaan_barang->count_all($table, $where, $joins),
            "recordsFiltered" => $this->M_penerimaan_barang->count_filtered($column, $table, $columnOrder, $columnSearch, $order, $where, $joins),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

   
    public function ajax_action_add()
    {

        $this->form_validation->set_rules('no_invoice', 'no invoice', 'required');
        $this->form_validation->set_rules('tanggal', 'tanggal', 'required');
        $this->form_validation->set_rules('supplier_id', 'supplier', 'required');
        $this->form_validation->set_rules('produk_id', 'produk', 'required');
        $this->form_validation->set_rules('harga', 'harga', 'required');
        $this->form_validation->set_rules('jumlah', 'jumlah', 'required');
        $this->form_validation->set_rules('catatan', 'catatan', 'required');

        $id = post("id");

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();

            return unprocessResponse(displayError($error), $error);
        } else {


            $data = array(
                "kode" => post("no_invoice"),
                "tanggal" => date("Y-m-d",strtotime(post("tanggal"))),
                "supplier_id" => post("supplier_id"),
                "produk_id" => post("produk_id"),
                "jumlah" => post("jumlah"),
                "harga" => post("harga"),
                "catatan" => post("catatan"),
                "jenis_stok" => "masuk"
            );
            if (isset($id) && !empty($id)) {
                $add = $this->M_penerimaan_barang->update_table("t_kartu_stok", $data,"id",$id);   
            }else{
                $add = $this->M_penerimaan_barang->insert_table("t_kartu_stok", $data);   
            }
            if ($add == false) {
                return unprocessResponse('Failed to save data');
            } else {
                return successResponse('Success Save', '' . base_url() . 'user/');
            }

        }

    }

   

}
