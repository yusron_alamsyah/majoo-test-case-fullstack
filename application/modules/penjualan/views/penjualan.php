<div class="row">
    <div class="col-md-12">
        <div class="btn-group pull-right" role="group" >
          <button type="button"  onclick="clickTab('sales')" id="button-sales" class="btn btn-sm btn-primary"><i class="fa fa-cart-plus"></i> Sales</button>
          <button type="button" onclick="clickTab('list')"  id="button-list" class="btn btn-sm btn-outline-primary"><i class="fa fa-list"></i> History</button>
        </div>
        <div class="mb-4"><h4 class="text-title"></h4></div>
    </div>
    <div class="col-md-12 show-sales">
        <div class="mt-1">&nbsp;</div>
        <table class="table table-borderless table-striped " id="list_data" >
            <thead style="background: #000; color: #FFF;">
                <tr>
                    <th>
                        <center>Kode</center>
                    </th>
                    <th>
                        <center>Customer</center>
                    </th>
                    <th>
                        <center>Operator</center>
                    </th>
                    <th>
                        <center>Total</center>
                    </th>
                    <th>
                        <center></center>
                    </th>
                </tr>
            </thead>
            <tbody>
            </tbody>
        </table>
    </div>
    <div class="col-md-6 show-list ">
        <div class="row">
            <?php foreach ($list_product as $key => $value) {?>
                <div class="col-md-6 mb-3">
                    <div class="p-2" style="background:#FFF; border:1px solid #28a745; border-radius: 5%;">
                            <img src="<?php echo base_url()."uploads/produk/".$value->gambar; ?>" style="height:150px; width:100%;">
                            <div class="mt-1 mb-1"><center><b><?php echo $value->nama; ?></b></center></div>
                            <div class="mt-1 mb-1"><center><?php echo number_format($value->harga); ?></center></div>
                            <button onclick="pilih(<?php echo $value->id; ?>,'<?php echo $value->nama; ?>',<?php echo $value->harga; ?>)" class="btn btn-block btn-outline-success">Pilih</button>
                        </div>
                </div>
            <?php }?>
        </div>
    </div>
    <div class="col-md-6  show-list pt-4" style="background: #fafafa; border-radius: 3%;">
            <div class="card-body p-3" >
                <form method="post" id="form-bayar">
                <div class="form-group">
                    <label class="required"><small>Customer :</small></label>
                    <select class="form-control select2" style="width: 100%;" name="pelanggan_id" id="pelanggan_id">
                        <option value="">Pilihan</option>
                        <?php foreach ($list_customer as $value) {?>
                            <option value="<?php echo $value->id; ?>"><?php echo $value->nama; ?> - <?php echo $value->no_hp; ?></option>
                        <?php }?>
                    </select>
                </div>
                <div class="form-group">
                    <label class="required"><small>Tanggal :</small></label>
                    <input type="text" class="form-control form-control-sm datetime" id="tanggal" name="tanggal">
                </div>
                </form>
            </div>
        <div class="table-responsive">
            <table class="table table-striped" style="zoom:80%;" id="list-pilihan">
                <tr class="bg-dark text-white">
                    <td style="width:5%;"></td>
                    <td style="width:1%;">No.</td>
                    <td>Nama</td>
                    <td class="text-right">Harga</td>
                    <td style="width:20%;" class="text-center">Jumlah</td>
                    <td class="text-right">Total</td>

                </tr>
                <tbody id="tbody">

                </tbody>
                <tr class="bg-dark text-white">
                    <td colspan="5" class="text-right">
                        Grand Total
                    </td>
                    <td class="text-success text-right" style="font-size:20px">
                        <b id="grandTotal">Rp.50.000</b>
                    </td>
                </tr>
            </table>
            <button onclick="bayar()" class="btn btn-success m-3 pull-right">BAYAR</button>
        </div>
    </div>
</div>


<script type="text/javascript">

var list_penjualan = [];
var index_awal = 0;


function clickTab(val){
    if (val == 'sales') {
        $(".text-title").html("Form Sales");

        $("#button-sales").removeClass("btn-outline-primary");
        $("#button-sales").addClass("btn-primary");

        $("#button-list").removeClass("btn-primary");
        $("#button-list").addClass("btn-outline-primary");

        $(".show-sales").fadeOut();
        $(".show-list").fadeIn();
    }else{
        
        
        $(".text-title").html("History Sales");


        $("#button-list").removeClass("btn-outline-primary");
        $("#button-list").addClass("btn-primary");

        $("#button-sales").removeClass("btn-primary");
        $("#button-sales").addClass("btn-outline-primary");

        $(".show-list").fadeOut();
        $(".show-sales").fadeIn();
    }
    
}
clickTab("sales");

function numberWithCommas(x) {
    return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}

function removeDetail(val){
    if (confirm('Are You sure delete this data?')) {
        var value = val.value;
        var attrid = val.id;
        var splitid = attrid.split("-");
        var getid = splitid[2];

        $.ajax({
            url: "<?php echo base_url(); ?>penjualan/ajax_add_detail/",
            type: 'POST',
            dataType: "json",
            data: {
                id: getid,
                list : list_penjualan,
                type : 'delete'
            },
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    list_penjualan = data.message.body;
                    renderTable(list_penjualan);
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
    });
    }
}

function hitungJumlah(val){
    var value = val.value;
    var attrid = val.id;
    var splitid = attrid.split("-");
    var getid = splitid[2];
    if (value == "") {
        value = 0;
    }
    console.log(value);
    list_penjualan[getid].jumlah = parseInt(value);
    list_penjualan[getid].total = parseInt(value) * parseInt(list_penjualan[getid].harga);
    renderTable(list_penjualan);

}

function renderTable(list_penjualan){

    var no = 1; var grandTotal = 0; var jumlah = 0;
    $("#tbody").html("");
    // console.log(list_penjualan);
    if (list_penjualan!=undefined) {
        for (var key in list_penjualan) {
            var value = list_penjualan[key];



            var tr = "<tr>";
            tr = tr + '<td><button id="hapus-detail-'+value.id_produk+'" class="btn btn-sm btn-danger"><i class="fa fa-trash"></i></button></td>';
            tr = tr + "<td>"+no+"</td>";
            tr = tr + "<td>"+value.nama_barang+"</td>";
            tr = tr + "<td class='text-right'>"+numberWithCommas(value.harga)+"</td>";
            tr = tr + '<td><input id="jumlah-detail-'+value.id_produk+'" class="form-control" value="'+value.jumlah+'"  form-control-sm" type="number" name=""></td>';
            tr = tr + "<td class='text-right'>"+numberWithCommas(value.total)+"</td>";
            tr = tr + "</tr>";

            no = parseInt(no) +1;
            jumlah = parseInt(jumlah) +1;
            grandTotal = parseInt(grandTotal) + parseInt(value.total);

            $("#tbody").append(tr);

            document.getElementById('jumlah-detail-'+value.id_produk).onkeyup = function() {hitungJumlah(this)};
            document.getElementById('jumlah-detail-'+value.id_produk).onchange = function() {hitungJumlah(this)};
            document.getElementById('hapus-detail-'+value.id_produk).onclick = function() {removeDetail(this)};


        };
    }


    //jika data kosong
    if (jumlah == 0) {
       $("#tbody").append("<tr><td colspan='6'><center>Data Kosong</cente></td></tr>");
    }

    $("#grandTotal").html(numberWithCommas(grandTotal));

}
renderTable();

function pilih(id,nama,harga){

    $.ajax({
            url: "<?php echo base_url(); ?>penjualan/ajax_add_detail/",
            type: 'POST',
            dataType: "json",
            data: {
                id: id,
                nama : nama,
                harga : harga,
                list : list_penjualan,
                type : 'add'
            },
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    list_penjualan = data.message.body;
                    renderTable(list_penjualan);
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
    });

}


function ajax_list() {

        $('#list_data').DataTable({

            "processing": true, //Feature control the processing indicator.
            "serverSide": true, //Feature control DataTables' server-side processing mode.
            "order": [], //Initial no order.
            "pageLength": 10,
            // Load data for the table's content from an Ajax source
            "ajax": {
                "url": "<?php echo base_url(); ?>/penjualan/ajax_list/",
                "type": "POST",
                "data": {
                    '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
                }

            },

            //Set column definition initialisation properties.
            "columnDefs": [{
                "targets": [0], //first column / numbering column
                "orderable": false, //set not orderable
            }, ],

        });
}
ajax_list();

function reload_list(){
    $('#list_data').DataTable().ajax.reload();
}

function reset_form(){
    document.getElementById("form-bayar").reset();
    $('#pelanggan_id').val(null).trigger('change');

    list_penjualan = [];
    renderTable(list_penjualan);
}

function ajax_get_detail(id){
    $.ajax({
            url: "<?php echo base_url(); ?>penjualan/ajax_get_detail/",
            type: 'GET',
            dataType: "json",
            data: {
                id: id
            },
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    
                    $('#modal_add').modal("toggle");
                    var header = data.message.body.header;
                    var list = data.message.body.detail;
                    $("#print-kode").html(header.kode);
                    $("#print-nama").html(header.nama);
                    

                    var no_print = 1; var tr = ""; var total_print = 0;
                    $("#print-list").html("");
                    for (var key in list) {
                        var value = list[key];
                        tr = "<tr>";
                        tr = tr + "<td>"+no_print+"</td>";
                        tr = tr + "<td>"+value.nama_produk+"</td>";
                        tr = tr + "<td>"+numberWithCommas(value.harga)+"</td>";
                        tr = tr + "<td>"+value.jumlah+"</td>";
                        tr = tr + "<td>"+numberWithCommas(value.total)+"</td>";
                        tr = tr + "</tr>";
                        
                        $("#print-list").append(tr);
                        no_print = no_print + 1;
                        total_print = parseInt(total_print) + parseInt(value.total);
                   }
                   $("#print-total").html(numberWithCommas(total_print));

                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
}

function bayar() {

    $.ajax({
        url: "<?php echo base_url() ?>penjualan/ajax_action_bayar/",
        type: 'POST',
        dataType: "json",
        data: $( "#form-bayar" ).serialize()+ "&detail="+JSON.stringify(list_penjualan)+"&<?php echo $this->security->get_csrf_token_name(); ?>="+"<?php echo $this->security->get_csrf_hash(); ?>",
        beforeSend: function() {
            $('#page-load').show();
        },
        success: function(data) {
            $('#page-load').hide();
            if (data.result) {

                toastr["success"](data.message.body);
                reset_form();
                reload_list();

            } else {
                toastr["error"](data.message.body);
            }

        },
        error: function(request, status, error) {
            $('#page-load').hide();
            toastr["error"]("Error, Please try again later");
        }
    });

    return false;
}

</script>