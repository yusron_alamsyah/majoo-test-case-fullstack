<div class="modal-dialog modal-lg" style="width: 100%;" role="document">
    <div class="modal-content p-4">
        <div class="row">
            <div class="col-md-6">
                <img src="https://majoo.id/assets/img/main-logo.png" style="width:50%;" alt="Majoo" />
            </div>
            <div class="col-md-6">
                <div class="pull-right" style="font-size:20px;">
                    <b id="print-kode">#KW121241</b>
                </div>
            </div>
            <div class="col-md-12" style="font-size:20px;">
                <center>
                    <b>KWITANSI</b>
                </center>
            </div>
        </div>
                <input type="text" style="display: none;" id="id" name="id">
                <div class="modal-body">
                        <table class="table table-borderless">
                            <tr>
                                <td style="width:10%;">
                                    <label class="required">Nama </label>
                                </td>
                                <td>
                                    : <span id="print-nama">Yusron</span>
                                </td>
                            </tr>
                            <tr>
                                <td style="width:10%;">
                                    <label class="required">Total </label>
                                </td>
                                <td>
                                    : <span id="print-total">Yusron</span>
                                </td>
                            </tr>
                        </table>
                        <table class="table">
                            <tr>
                                <td>No</td>
                                <td>Produk</td>
                                <td>Harga</td>
                                <td>Jumlah</td>
                                <td>Sub Total</td>
                            </tr>
                            <tbody id="print-list">

                            </tbody>
                        </table>
                </div>
                <div class="modal-footer">
                    <a ><button type="button" data-dismiss="modal" class="btn btn-danger">Back</button></a>
                </div>
    </div>
</div>