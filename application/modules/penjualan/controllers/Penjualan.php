<?php defined('BASEPATH') or exit('No direct script access allowed');

class Penjualan extends MY_Controller
{

    public function __construct()
    {
        // Load the constructer from MY_Controller
        parent::__construct();

        $this->load->helper('url');
        $this->load->model("M_penjualan");
        $this->load->library('session');
        $this->load->library(array('session', 'pagination', 'form_validation'));
    }

    public function index()
    {

        cekLogin();
        $data['content'] = 'penjualan';
        $data['form']    = 'form';

        $data["list_product"] = $this->M_penjualan->fetch_table("m_produk.id,m_produk.nama,m_produk.harga,m_produk.gambar,", "m_produk", "");

        $data["list_customer"] = $this->M_penjualan->fetch_table("m_pelanggan.*,", "m_pelanggan", "");

        $this->load->view('login/home_page.php', $data);
    }

    public function ajax_add_detail()
    {
        $detail = post("list");
        if (empty($detail)) {
            $detail = [];
        }

        if (post("type") == 'add') {
            if ($detail[post("id")]) {
                $jumlah = $detail[post("id")]["jumlah"] + 1;
            } else {
                $jumlah = 1;
            }
            $total              = $jumlah * post("harga");
            $detail[post("id")] = array(
                "id_produk"   => post("id"),
                "nama_barang" => post("nama"),
                "harga"       => post("harga"),
                "jumlah"      => $jumlah,
                "total"       => $total,
            );
        } else if (post("type") == 'delete') {
            if (isset($detail[post("id")])) {
                unset($detail[post("id")]);
            }
        }

        return successResponse($detail, '');
    }
    public function ajax_list()
    {
        $column        = 'm_pelanggan.nama,t_penjualan.*,m_admin.username as nama_admin';
        $columnOrder  = array(null, 'kode', 'tanggal', "m_pelanggan.nama"); //set column field database for datatable orderable
        $columnSearch = array('kode', 'tanggal', "m_pelanggan.nama", "m_admin.username"); //set column field database for datatable searchable
        $order         = array('id' => 'DESC'); // default order
        $table         = "t_penjualan";
        $where         = "";
        $joins         = array(
            array(
                'table'     => 'm_pelanggan',
                'condition' => 'm_pelanggan.id = t_penjualan.pelanggan_id',
                'jointype'  => '',
            ),
            array(
                'table'     => 'm_admin',
                'condition' => 'm_admin.id = t_penjualan.admin_id',
                'jointype'  => '',
            ),
        );

        $list = $this->M_penjualan->get_datatables($column, $table, $columnOrder, $columnSearch, $order, $where, $joins);

        $data = array();
        $no   = $_POST['start'];
        foreach ($list as $key) {
            $no++;
            $row   = array();
            $row[] = "<div class='text-success'><b>" . $key->kode . "</b></div><small>Date : " . date("d-m-Y", strtotime($key->tanggal)) . "</small>";
            $row[] = $key->nama;
            $getTotal = $this->M_penjualan->fetch_table("sum(total) as total", "t_penjualan_det", "t_penjualan_det.penjualan_id = '" . $key->id . "' ");

            $total = isset($getTotal[0]->total) ? number_format($getTotal[0]->total) : 0;
            $row[] = $key->nama_admin;
            $row[] = $total;

            $row[] = "
                    <center>
                    <button data-toggle='tooltip' data-placement='top' title='Print' 
                    onclick='ajax_get_detail($key->id)' class='btn btn-success btn-sm'><i class='zmdi zmdi-print'></i></button>
                    </center>";

            $data[] = $row;
        }

        $output = array(
            "draw"            => $_POST['draw'],
            "recordsTotal"    => $this->M_penjualan->count_all($table, $where, $joins),
            "recordsFiltered" => $this->M_penjualan->count_filtered($column, $table, $columnOrder, $columnSearch, $order, $where, $joins),
            "data"            => $data,
        );
        //output to json format
        echo json_encode($output);
    }

    public function ajax_get_detail()
    {

        $id     = get("id");
        $joins = array(
            array(
                'table'     => 'm_pelanggan',
                'condition' => 'm_pelanggan.id = t_penjualan.pelanggan_id',
                'jointype'  => '',
            ),
        );
        $header = $this->M_penjualan->fetch_joins("t_penjualan.*,m_pelanggan.nama", "t_penjualan", $joins, "t_penjualan.id = '" . get('id') . "'");

        $joins = array(
            array(
                'table'     => 'm_produk',
                'condition' => 'm_produk.id = t_penjualan_det.produk_id',
                'jointype'  => '',
            ),
        );
        $detail = $this->M_penjualan->fetch_joins("t_penjualan_det.*,m_produk.nama as nama_produk", "t_penjualan_det", $joins, "penjualan_id = '" . get('id') . "'");
        if ($header) {
            $return = array(
                "header" => $header[0],
                "detail" => $detail,
            );
            return successResponse($return);
        } else {
            return unprocessResponse('Failed to get data');
        }
    }
    public function ajax_action_bayar()
    {

        $this->form_validation->set_rules('pelanggan_id', 'Customer', 'required');
        $this->form_validation->set_rules('tanggal', 'Tanggal', 'required');

        if ($this->form_validation->run() == false) {
            $error = $this->form_validation->error_array();

            return unprocessResponse(displayError($error), $error, '');
        } else {

            $detail = json_decode(post("detail"));
            if (empty($detail)) {
                return unprocessResponse("Detail Penjualan Kosong");
            }

            $errorStok = [];
            foreach ($detail as $key => $value) {
                $getStok =  $this->M_penjualan->fetch_table("sum(jumlah) as stok", "t_kartu_stok", "produk_id = '" . $value->id_produk . "' ");
                $sisa_stok = isset($getStok[0]->stok) ? $getStok[0]->stok : 0;
                if ($sisa_stok < $value->jumlah) {
                    $errorStok[] = "Sisa Stok Produk " . $value->nama_barang . " " . $sisa_stok;
                }
            }
            if (!empty($errorStok)) {
                return unprocessResponse(displayError($errorStok), $errorStok, '');
            }

            $getLast = $this->M_penjualan->fetch_table("kode", "t_penjualan", "MONTH(tanggal) = '" . date("m") . "' AND YEAR(tanggal) = '" . date("Y") . "' ", "kode", "desc", $start = 0, 1);

            if (empty($getLast)) {
                $no_urut = 1;
            } else {
                $no_urut = substr($getLast[0]->kode, 6) + 1;
            }

            $kode = generateTransactionCode($no_urut, "KW");
            // echo $no_urut; die();

            $data = array(
                "kode"         => $kode,
                "pelanggan_id" => post("pelanggan_id"),
                "tanggal"      => date("Y-m-d", strtotime(post("tanggal"))),
                "admin_id"     => $_SESSION['admin_id'],
            );

            $add = $this->M_penjualan->insert_table("t_penjualan", $data);

            foreach ($detail as $key => $value) {
                $data_detail = array(
                    "produk_id"    => $value->id_produk,
                    "penjualan_id" => $add,
                    "harga"        => $value->harga,
                    "jumlah"       => $value->jumlah,
                    "total"        => $value->total,
                );
                $addDetail = $this->M_penjualan->insert_table("t_penjualan_det", $data_detail);

                //insert kartu stok
                $data_stok = array(
                    "kode" => $kode,
                    "tanggal" => date("Y-m-d", strtotime(post("tanggal"))),
                    "customer_id" => post("pelanggan_id"),
                    "produk_id" => $value->id_produk,
                    "jumlah" => '-' . $value->jumlah,
                    "harga" => $value->harga,
                    "catatan" => "Penjualan " . $kode . " | " . post('tanggal'),
                    "jenis_stok" => "keluar"
                );
                $addStok = $this->M_penjualan->insert_table("t_kartu_stok", $data_stok);
            }



            if ($add == false) {
                return unprocessResponse('Failed to save data');
            } else {
                return successResponse('Success Bayar', '' . base_url() . 'user/');
            }
        }
    }
}
