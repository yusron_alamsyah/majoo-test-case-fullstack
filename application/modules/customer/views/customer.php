<button onclick="reset_form()" data-toggle="modal" data-target="#modal_add" class="btn pull-right btn-primary btn-sm">
    <i class="fas fa-plus"></i>
    New Customer
</button>
<div class="mb-1">
    <h4 class="text-title">Customer</h4>
</div>
<br><br>
<table class="table table-borderless table-striped " id="list_data">
    <thead style="background: #000; color: #FFF;">
        <tr>
            <th>
                <center>No</center>
            </th>
            <th>
                <center>Nama</center>
            </th>
            <th>
                <center>No. HP</center>
            </th>
            <th>
                <center>Alamat</center>
            </th>
            <th>
                <center>Action</center>
            </th>
        </tr>
    </thead>
    <tbody>
    </tbody>
</table>



<script type="text/javascript">
function ajax_list() {

    $('#list_data').DataTable({

        "processing": true, //Feature control the processing indicator.
        "serverSide": true, //Feature control DataTables' server-side processing mode.
        "order": [], //Initial no order.
        "pageLength": 10,
        // Load data for the table's content from an Ajax source
        "ajax": {
            "url": "<?php echo base_url(); ?>/customer/ajax_list/",
            "type": "POST",
            "data": {
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            }

        },

        //Set column definition initialisation properties.
        "columnDefs": [{
            "targets": [0], //first column / numbering column
            "orderable": false, //set not orderable
        }, ],

    });
}
ajax_list();

function reload_list() {
    $('#list_data').DataTable().ajax.reload();
}

function reset_form() {
    $(".info-password").addClass("d-none");

    document.getElementById("form-add").reset();
}

function ajax_get_edit(id) {
    $.ajax({
        url: "<?php echo base_url(); ?>customer/ajax_get_edit/",
        type: 'GET',
        dataType: "json",
        data: {
            id: id
        },
        beforeSend: function() {
            $('#page-load').show();
        },
        success: function(data) {
            $('#page-load').hide();
            if (data.result) {
                var list = data.message.body;
                $('#modal_add').modal("toggle");
                $("#nama").val(list.nama);
                $("#no_hp").val(list.no_hp);
                $("#alamat").val(list.alamat);
                $("#id").val(list.id);
            } else {
                toastr["error"](data.message.body);
            }

        },
        error: function(request, status, error) {
            $('#page-load').hide();
            toastr["error"]("Error, Please try again later");
        }
    });
}

function ajax_action_add() {

    $.ajax({
        url: "<?php echo base_url() ?>customer/ajax_action_add/",
        type: 'POST',
        dataType: "json",
        data: $("#form-add").serialize() + "&<?php echo $this->security->get_csrf_token_name(); ?>=" +
            "<?php echo $this->security->get_csrf_hash(); ?>",
        beforeSend: function() {
            $('#page-load').show();
        },
        success: function(data) {
            $('#page-load').hide();
            if (data.result) {

                toastr["success"](data.message.body);
                $('#modal_add').modal("toggle");
                reload_list();

            } else {
                toastr["error"](data.message.body);
            }

        },
        error: function(request, status, error) {
            $('#page-load').hide();
            toastr["error"]("Error, Please try again later");
        }
    });

    return false;
}

function ajax_action_delete(id) {
    if (confirm('Are You sure delete this data?')) {
        $.ajax({
            url: "<?php echo base_url(); ?>/customer/ajax_action_delete/",
            type: 'POST',
            dataType: "json",
            data: {
                id: id,
                '<?php echo $this->security->get_csrf_token_name(); ?>': '<?php echo $this->security->get_csrf_hash(); ?>'
            },
            beforeSend: function() {
                $('#page-load').show();
            },
            success: function(data) {
                $('#page-load').hide();
                if (data.result) {
                    toastr["success"](data.message.body);
                    reload_list();
                } else {
                    toastr["error"](data.message.body);
                }

            },
            error: function(request, status, error) {
                $('#page-load').hide();
                toastr["error"]("Error, Please try again later");
            }
        });
    }
}
</script>